2
00:00:00 --> 00:00:25
cut to high angle ELS Bus 34right while Bus moves to screen bottom right

3
00:00:25 --> 00:00:31
then as Bus stops near Dirt road and Thornhill moves to Dirt road

4
00:00:31 --> 00:00:33
hold to high angle ELS Thornhill left ELS Bus 34right

5
00:00:33 --> 00:00:44
then as Bus exits screen bottom right 

6
00:00:44 --> 00:00:49
hold to high angle ELS Thornhill 34backright

7
00:00:51 --> 00:00:54
cut to LS Thornhill 34right

8
00:00:56 --> 00:00:58
cut to ELS Bus 34backright while Bus moves to screen top right

9
00:01:00 --> 00:01:04
cut to MS Thornhill 34left then as Thornhill turns right hold to MS Thornhill 34right

10
00:01:04 --> 00:01:07
cut to ELS Corn field


11
00:01:07 --> 00:01:10
cut to MS Thornhill 34right then Thornhill looks at screen top right

12
00:01:10 --> 00:01:13
cut to ELS Highway

13
00:01:14 --> 00:01:17
cut to MS Thornhill 34right then as Thornhill turns away from camera hold to MS Thornhill 34backright

14
00:01:17 --> 00:01:20
cut to ELS Plane right then Plane moves to screen right

15
00:01:20 --> 00:01:23
cut to MS Thornhill 34backright then as Thornhill turns towards camera

16
00:01:23 --> 00:01:26
hold to MS Thornhill front

17
00:01:27 --> 00:01:30
cut to Corn field

18
00:01:30 --> 00:01:32
cut to MS Thornhill front then as Thornhill looks screen right hold to MS Thornhill 34right

19
00:01:32 --> 00:01:35
cut to ELS White car 34right while White car moves to screen bottom right

20
00:01:36 --> 00:01:37
cut to MS Thornhill 34right while Thornhill looks screen right

21
00:01:37 --> 00:01:38
cut to LS White car 34right while White car moves to screen bottom right

22
00:01:38 --> 00:01:39
then quick pan right to MS White car right

23
00:01:39 --> 00:01:41
cut to MS Thornhill 34right then as Thornhill turns left hold to MS Thornhill 34left

24
00:01:41 --> 00:01:45
cut to LS White car 34backright while White car moves to screen top right

25
00:01:45 --> 00:01:49
cut to MS Thornhill 34left then as Thornhill turns right hold to MS Thornhill 34right

26
00:01:49 --> 00:01:52
cut to Highway

27
00:01:52 --> 00:01:55
cut to MS Thornhill 34right then as Thornhill turns left hold to MS Thornhill 34left

28
00:01:55 --> 00:01:58
cut to ELS Limo 34left while Limo moves to screen bottom left

29
00:01:59 --> 00:02:01
cut to MS Thornhill 34left while Thornhill looks screen left

30
00:02:01 --> 00:02:05
cut to ELS Limo 34left while Limo moves to screen bottom left then hold to LS Limo 34left

31
00:02:05 --> 00:02:08
cut to MS Thornhill 34left while Thornhill looks screen left

32
00:02:08 --> 00:02:10
cut to LS Limo 34left while Limo moves to screen bottom left then pan left to FS Limo left

33
00:02:10 --> 00:02:13
cut to MS Thornhill front then as Thornhill turns right hold to MS Thornhill 34right

34
00:02:13 --> 00:02:16
cut to LS Limo 34backleft while Limo moves to screen top left

35
00:02:16 --> 00:02:19
cut to MS Thornhill 34right then Thornhill looks screen top right

36
00:02:19 --> 00:02:22
cut to ELS Truck 34right while Truck moves to screen bottom right

38
00:02:22 --> 00:02:26
cut to MS Thornhill 34right while Thornhill looks screen right

39
00:02:26 --> 00:02:29
cut to ELS Truck 34right while Truck moves to screen bottom right then hold to LS Truck 34right

40
00:02:29 --> 00:02:31
cut to MS Thornhill 34right while Thornhill looks screen right

41
00:02:31 --> 00:02:32
cut to low angle LS Thornhill 34right

42
00:02:32 --> 00:02:33
then as Truck enters screen bottom right and Truck moves to Thornhill hold to low angle LS Thornhill 34right FS Truck 34left

43
00:02:33 --> 00:02:35
then as Truck crosses over Thornhill and Thornhill turns left hold to low angle LS Thornhill front

44
00:02:35 --> 00:02:42
cut to MS Thornhill 34left then as Thornhill turns towards camera hold to MS Thornhill front

45
00:02:42 --> 00:02:48
cut to Corn field then as Blue car exits Corn field and Blue car moves to screen top left hold to ELS Blue car 34backleft

46
00:02:48 --> 00:02:50
cut to MS Thornhill front while Thornhill looks screen right

47
00:02:50 --> 00:02:54
cut to ELS Blue car left then as Blue car turns towards camera hold to LS Blue car front

48
00:02:54 --> 00:02:57
cut to MS Thornhill front while Thornhill looks screen right

49
00:02:57 --> 00:03:01
cut to LS Blue car front while Blue car moves to screen bottom right then hold to LS Blue car 34right

50
00:03:01 --> 00:03:04
cut to MS Thornhill front while Thornhill looks screen right

51
00:03:04 --> 00:03:08
cut to LS Blue car 34right while Blue car moves to screen bottom right then dolly right to LS Blue car

52
00:03:08 --> 00:03:10
cut to MS Thornhill front while Thornhill looks screen right

53
00:03:10 --> 00:03:14
cut to LS MBS Blue car 34right then as Blue car moves to screen top left and MBS crosses over Blue car hold to LS Blue car MBS 34right

55
00:03:14 --> 00:03:16
cut to MS Thornhill front while Thornhill looks screen right

56
00:03:16 --> 00:03:18
cut to LS Blue car 34left LS MBS 34right then Blue car moves to screen top right

57
00:03:18 --> 00:03:19
cut to MCU Thornhill front

58
00:03:19 --> 00:03:23
cut to LS Blue car 34left LS MBS 34right then as Blue car exits screen left dolly right to LS MBS 34right

59
00:03:23 --> 00:03:27
cut to MCU Thornhill front while Thornhill looks screen right

60
00:03:27 --> 00:03:33
cut to LS Thornhill 34right LS MBS 34left then MBS looks screen top right

61
00:03:33 --> 00:03:36
cut to MCU Thornhill front while Thornhill looks screen right

62
00:03:39 --> 00:03:41
cut to LS MBS 34right while MBS looks screen left

63
00:03:42 --> 00:03:45
cut to MS Thornhill front then as Thornhill turns right hold to MS Thornhill 34right

64
00:03:45 --> 00:03:51
then as Thornhill moves to screen right dolly right with Thornhill

65
00:03:51 --> 00:03:54
cut to LS MBS 34right then dolly in with MBS

66
00:03:54 --> 00:03:56
cut to dolly right with MS Thornhill 34right while Thornhill moves to screen right

67
00:03:56 --> 00:03:58
cut to LS MBS 34right then dolly in to FS MBS 34right

68
00:03:58 --> 00:04:01
cut to dolly right with MS Thornhill 34right while Thornhill moves to screen right

69
00:04:01 --> 00:04:06
then as MBS appears screen right dolly right to

70
00:04:06 --> 00:04:08
MS Thornhill 34right MS MBS 34left then Thornhill speaks to MBS


72
00:04:18 --> 00:04:21
cut to ELS Plane right while Plane moves to screen right

73
00:04:21 --> 00:04:25
cut to MS Thornhill front MS MBS 34left then as Thornhill turns right hold to MS Thornhill 34right MS MBS 34left

74
00:04:29 --> 00:04:31
then as MBS looks screen top right hold to MS Thornhill 34right MS MBS 34left

75
00:04:31 --> 00:04:33
cut to ELS Green bus front while Green bus moves to screen bottom right

76
00:04:33 --> 00:04:39
cut to MS Thornhill 34right MS MBS 34left

77
00:04:39 --> 00:04:40
then as MBS looks screen left and Thornhill turns towards camera hold to MS Thornhill front MBS 34left

78
00:04:40 --> 00:04:44
cut to ELS Plane right while Plane moves to screen right

79
00:04:44 --> 00:04:47
cut to MLS Thornhill MBS 34left then MBS  Thornhill looks screen left

80
00:04:47 --> 00:04:49
cut to FS Green bus 34right while Green bus moves to screen bottom right

81
00:04:49 --> 00:04:51
cut to MS Green bus right MLS Thornhill  MS MBS 34left then as MBS crosses over Thornhill and MBS moves to Green bus

82
00:04:51 --> 00:04:52
hold to MS Green bus MLS MBS 34backleft MLS Thornhill 34left

83
00:04:55 --> 00:04:58
then as Green bus moves to screen top left and Green bus crosses under Thornhill

84
00:04:58 --> 00:05:02
hold to MLS Thornhill 34backleft LS Green bus 34backleft

85
00:05:04 --> 00:05:10
then as Thornhill turns left hold to MLS Thornhill 34left LS Green bus 34backleft

86
00:05:11 --> 00:05:14
cut to ELS Plane right then as Plane turns towards camera dolly right to ELS Plane 34right

87
00:05:15 --> 00:05:16
cut to MS Thornhill front while Thornhill looks screen top

88
00:05:17 --> 00:05:21
cut to ELS Plane 34right then as Plane turns towards camera dolly right to LS Plane front

89
00:05:21 --> 00:05:23
cut to MS Thornhill front while Thornhill looks screen top

90
00:05:23 --> 00:05:25
cut to pan down with LS Plane front while Plane moves to screen center

90
00:05:25 --> 00:05:29
cut to high angle MS Thornhill front while Thornhill looks screen top

91
00:05:29 --> 00:05:30
cut to LS Plane front while Plane moves to screen center then hold to FS Plane front

92
00:05:30 --> 00:05:31
cut to high angle MS Thornhill front then Thornhill turns right

93
00:05:31 --> 00:05:32
cut to low angle FS Thornhill 34left FS Plane front

94
00:05:32 --> 00:05:34
then as Thornhill moves to Dirt road and Plane exits screen top hold to low angle FS Thornhill front

95
00:05:34 --> 00:05:37
cut to low angle LS Plane back while Plane moves to screen center then as Plane turns right hold to ELS Plane back

96
00:05:37 --> 00:05:40
cut to FS Thornhill 34right then Thornhill looks screen right

97
00:05:40 --> 00:05:43
cut to low angle LS Plane 34backleft then as Plane turns right dolly left to ELS Plane 34backright

98
00:05:43 --> 00:05:45
cut to FS Thornhill 34right then as Thornhill looks screen right pan up with Thornhill

99
00:05:45 --> 00:05:47
cut to LS Plane 34right then as Plane moves to screen bottom right and Plane turns towards camera dolly right to LS Plane front

100
00:05:47 --> 00:05:49
cut to MLS Thornhill front then as Thornhill turns right hold to MLS Thornhill 34right

101
00:05:49 --> 00:05:50
cut to LS Plane front while Plane moves to screen center then hold to FS Plane front

102
00:05:50 --> 00:05:52
cut to MLS Thornhill front then as Thornhill moves to screen bottom left quick dolly left to MLS Thornhill left

103
00:05:52 --> 00:05:56
cut to MLS Thornhill right low angle MS Plane front then as Plane exits screen top hold to MLS Thornhill right

104
00:05:57 --> 00:06:00
cut to low angle LS Plane 34backleft then as Plane turns left dolly left with Plane

105
00:06:00 --> 00:06:02
then as Plane turns right dolly left to LS Plane 34backright

106
00:06:02 --> 00:06:04
cut to MLS Thornhill 34right then Thornhill looks screen bottom left

107
00:06:04 --> 00:06:06
cut to LS Bluewhite car 34left while Bluewhite car moves to screen bottom left

108
00:06:07 --> 00:06:08
cut to MLS Thornhill 34right

109
00:06:08 --> 00:06:10
cut to low angle FS Thornhill 34backleft then as Thornhill moves to Highway pan up to

110
00:06:10 --> 00:06:12
low angle LS Thornhill 34backright Bluewhite car 34left

111
00:06:12 --> 00:06:15
then as Thornhill looks at Bluewhite car and Bluewhite car moves to Thornhill dolly left to

112
00:06:15 --> 00:06:17
low angle LS Thornhill 34backleft Bluewhite car 34left

113
00:06:17 --> 00:06:19
cut to FS Bluewhite car 34left  MLS Thornhill 34backleft

114
00:06:19 --> 00:06:21
then as Bluewhite car exits screen bottom left and Thornhill looks screen left hold to MLS Thornhill 34left

115
00:06:21 --> 00:06:23
cut to ELS Plane 34right while Plane moves to screen right

116
00:06:23 --> 00:06:26
cut to low angle ELS Plane 34right MLS Thornhill 34backleft then as Plane moves to Thornhill and Thornhill turns left

117
00:06:26 --> 00:06:29
hold to low angle ELS Plane front  MLS Thornhill left

118
00:06:29 --> 00:06:34
then as Thornhill moves to screen center and Plane moves to Thornhill dolly out to low angle FS Plane front MS Thornhill front

119
00:06:34 --> 00:06:36
then as Plane exits screen top and Thornhill moves to Dirt road dolly out to low angle MS Thornhill front

120
00:06:36 --> 00:06:40
cut to MLS Thornhill right then as Thornhill looks at screen top right hold to MLS Thornhill 34right

121
00:06:40 --> 00:06:42
cut to Corn field

122
00:06:42 --> 00:06:43
cut to MLS Thornhill 34right

123
00:06:43 --> 00:06:44
cut to low angle FS Thornhill right LS Plane 34backleft

124
00:06:44 --> 00:06:46
then as Plane moves to screen top left pan up to low angle LS Plane 34left FS Thornhill right then Thornhill moves to screen right

126
00:06:46 --> 00:06:51
cut to quick dolly right with MLS Thornhill 34right while Thornhill moves to screen right

127
00:06:51 --> 00:06:54
cut to FS Thornhill back while Thornhill moves to Corn field then hold to LS Thornhill back

128
00:06:54 --> 00:06:55
cut to high angle LS Thornhill back then Thornhill enters Corn field

129
00:06:56 --> 00:07:00
cut to low angle FS Thornhill 34right then as Thornhill moves to Corn field pan down to FS Thornhill front

130
00:07:00 --> 00:07:02
then as Thornhill turns right hold to FS Thornhill 34right

131
00:07:02 --> 00:07:06
cut to low angle LS Plane front while Plane moves to screen center then Plane exits screen top right

132
00:07:07 --> 00:07:15
cut to FS Thornhill 34right then Thornhill looks screen top right

133
00:07:21 --> 00:07:24
cut to low angle ELS Plane left then as Plane turns left and Plane moves to Corn field hold to low angle ELS Plane front

134
00:07:24 --> 00:07:28
cut to FS Thornhill 34right then as Thornhill turns left and Thornhill looks screen top left hold to FS Thornhill 34left

135
00:07:28 --> 00:07:34
cut to low angle LS Plane front while Plane moves to screen center then Plane exits screen top right

136
00:07:35 --> 00:07:39
cut to FS Thornhill 34left then as Thornhill turns right hold to FS Thornhill 34right

137
00:07:43 --> 00:07:46
then as Thornhill turns away from camera pan up to MLS Thornhill back

138
00:07:46 --> 00:07:50
cut to MLS Thornhill 34left then as Thornhill moves to screen bottom left dolly left to MCU Thornhill front

139
00:07:51 --> 00:07:53
cut to ELS Oil truck 34left while Oil truck moves to screen bottom left

140
00:07:53 --> 00:07:56
cut to MCU Thornhill 34left then Thornhill exits screen bottom left

141
00:07:56 --> 00:07:57
cut to LS Thornhill 34backleft  ELS Oil truck 34left

142
00:07:57 --> 00:08:00
then as Thornhill moves to Highway and Oil truck moves to screen bottom left hold to ELS Thornhill 34backleft ELS Oil truck 34left

143
00:08:00 --> 00:08:02
cut to dolly left with ELS Plane left while Plane moves to screen left

144
00:08:02 --> 00:08:03
cut to low angle FS Thornhill 34backright LS Oil truck front then  Oil truck moves to Thornhill

145
00:08:03 --> 00:08:06
cut to ELS Plane left then as Plane turns towards camera dolly left to ELS Plane 34left

146
00:08:06 --> 00:08:08
cut to MS Thornhill front then Thornhill looks screen left

147
00:08:08 --> 00:08:09
cut to LS Oil truck front while Oil truck moves to screen center

148
00:08:09 --> 00:08:11
cut to MS Thornhill front then Thornhill looks screen left

149
00:08:11 --> 00:08:12
cut to FS Oil truck front

150
00:08:12 --> 00:08:13
cut to MS Thornhill front

151
00:08:13 --> 00:08:14
cut to ECU Oil truck

152
00:08:14 --> 00:08:15
cut to CU Thornhill front then quick zoom in to ECU Thornhill front

153
00:08:15 --> 00:08:16
cut to low angle CU Oil truck front FS Thornhill back then as Thornhill moves to Highway and Oil truck moves to Thornhill pan down to ECU Oil truck front FS Thornhill 34backleft

154
00:08:16 --> 00:08:17
cut to MS Thornhill 34right ECU Oil truck 34left then Thornhill looks screen bottom left

155
00:08:17 --> 00:08:19
cut to low angle ECU Oil truck 34right LS Plane 34left then as Plane moves to Oil truck pan up to low angle ECU Oil truck 34right FS Plane front

156
00:08:19 --> 00:08:19
cut to high angle LS Oil truck 34left LS Plane 34backleft then Plane moves to Oil truck

157
00:08:19 --> 00:08:22
cut to FS Oil truck 34left LS Thornhill 34backright FS Plane left then Oil truck reacts to Plane

158
00:08:22 --> 00:08:23
cut to MLS Oil truck 34left FS Thornhill 34backright FS Plane left

159
00:08:23 --> 00:08:25
then as Thornhill touches Truck and TD1 speaks to Thornhill hold to

160
00:08:25 --> 00:08:27
MLS Oil truck 34left FS Thornhill 34backright FS TD2 front FS TD1 34left FS Plane left

161
00:08:27 --> 00:08:28
then as TD1 TD2 exits screen right and Thornhill turns left hold to MLS Oil truck 34left FS Thornhill right FS Plane left

162
00:08:28 --> 00:08:30
cut to TD2 34backright TD1 back while TD1 TD2 moves to Corn field

163
00:08:30 --> 00:08:32
cut to FS Oil truck 34left MS Thornhill 34left then as Thornhill moves to screen center dolly out to LS Oil truck 34left MS Thornhill 34left

164
00:08:32 --> 00:08:35
cut to MLS Thornhill 34right LS Pickup 34left LS Brown car 34left

165
00:08:35 --> 00:08:40
then as Thornhill crosses over Pickup and Farmer BC Driver moves to Thornhill hold to LS Pickup 34left ELS Thornhill 34backright Farmer BC Driver BC Woman BC Man front

166
00:08:40 --> 00:08:42
cut to LS Oil truck 34left LS Plane left ECU Farmer back

167
00:08:42 --> 00:08:45
cut to low angle FS Farmer left FS Thornhill 34left BC Driver left BC Woman 34left BC Man 34backleft

168
00:08:45 --> 00:08:50
then as Thornhill crosses under BC Driver BC Man BC Woman hold to low angle FS Farmer 34backleft BC Driver left BC Woman 34left BC Man 34backleft Thornhill left

169
00:08:50 --> 00:08:52
then as Thornhill exits screen right and Farmer exits screen left hold to low angle BC Driver left BC Woman 34left BC Man left

170
00:08:52 --> 00:08:54
cut to LS Oil truck 34left LS Plane left MLS Farmer BC Man BC Woman BC Driver back

171
00:08:54 --> 00:08:56
then as Farmer moves to Oil truck and BC Driver crosses under BC Woman

172
00:08:56 --> 00:08:58
hold to LS Oil truck 34left LS Plane left FS Farmer 34backright BC Man 34backright BC Driver back MLS BC Woman 34backleft

173
00:08:58 --> 00:09:02
cut to LS Pickup front LS Brown car 34left then as Pickup turns right hold to LS Pickup 34backright LS Brown car 34left

174
00:09:02 --> 00:09:04
cut to LS Oil truck 34left LS Plane left FS Farmer right BC Man back BC Driver back MLS BC Woman 34backleft then Farmer exits screen right

175
00:09:04 --> 00:09:06
cut to LS Brown car 34left LS Farmer back LS Pickup back

176
00:09:06 --> 00:09:15
then as Farmer moves to Pickup and Pickup moves to screen top center hold to LS Brown car 34left ELS Pickupback ELS Farmer back






