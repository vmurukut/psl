from parsimonious.grammar import Grammar, RuleVisitor
from parsimonious.nodes import NodeVisitor

def PSL_parse(psl_sentence,input_script):  
   main = (""" 
       Scene            = Shot+    
       Shot             = Timing? _ Transition _ CameraWith? _ Composition _ WhileEvent?  _ Development* _  
       WhileEvent       = Timing? _ "while" _ Event 
       Development      = Timing? _ "then" _ (Recomposition / Continuation)
       Continuation     = Event / FollowEvent 
       Recomposition    = Cue?  _ CameraTo _ Composition 
       FollowEvent      = Cue? _ CameraWith _ Agent _ ("and"? _ Agent)* 
       Cue              = Timing? _ "as" _ Event _ ("and"? _ Event)* 
       Composition      = Timing? _ (Angle? _ Figure)+ 
       Figure           = Size? _ Subject _ Profile? _ Screen?   
       Agent            = ((Actor / Object) _ )+ 
       Subject          = ((Actor / Object / Place) _ )+
       CameraWith       = Timing? _ Speed? _ (Pan / Dolly / Crane)  _ "with" 
       CameraTo         = Timing? _ (Hold /(Speed? _ (Pan / Dolly / Crane / Zoom))) _ "to" 
       Hold             = "hold"
       Pan              = "pan"   _ ("left" / "right" / "up" / "down")?  
       Dolly            = "dolly" _ ("left" / "right" / "in" / "out")?
       Crane            = "crane"  _ ("up" / "down") _ ("left"/"right")? 
       Zoom             = "zoom" _ ("in" / "out")
       Speed            = "slow" / "quick" 
       Transition       = ("cut to" / "dissolve to" / "fade in to")
       Timing           = "from time to time" / ~"[0-9]* [0-9][0-9]:[0-9][0-9]:[0-9][0-9] --> [0-9][0-9]:[0-9][0-9]:[0-9][0-9]" 

       
       Enter            = "enters"  _ Screen? _ Place?   
       Exit             = "exits" _ Screen? _ Place?
       Look             = "looks" _ "at"? _ (Subject / Screen) 
       Move             = "moves" _ "to" _ (Subject / Screen )  
       Speak            = ("speaks" / ("says" _ String)) _ ("to" _ Subject)? 
       Use              = "uses" _ Object 
       Cross            = "crosses" _ ("over" / "under") _ Subject
       Touch            = "touches" _ Subject 
       React            = "reacts to" _ Subject
       Turn             = "turns" _ ("left" / "right" / "towards camera" / "away from camera")
       Stop             = "stops" _ ("at" / "near") _ Subject
       Appear           = "appears" _ (Screen / ("from behind" _ Subject))
       Disappear        = "disappears" _ (Screen/ ("behind" _ Subject))
       
       Angle            = "low angle" / "high angle" 
       Size             = "ECU" / "CU" / "MCU" / "MS" / "MLS" / "FS" / "LS" / "ELS"
       Profile          = "left" / "right" / "front" / "back" / "34left" / "34right" / "34backleft" / "34backright"
       Screen           = "screen" _ ("top" / "bottom")? _ ("left" / "center"/ "right")?
       Time             = ~r"[0-9]*"i _ ("seconds" / "s" )
       String           = ~r"[A-Z 0-9]*"i
       Action           = Enter / Exit / Look / Move / Speak / Use / Cross / Touch / React / Turn / Stop / Appear / Disappear
       Event            = Agent _ Action 
       space            = ~r"\s"
       _                = (space)*
       """)
   
   script_elements_NNW = ("""
            Actor       = "Thornhill" / "MBS" / "Plane" / "TD1" / "TD2" / "Farmer" / "BC Driver" / "BC Woman" / "BC Man"
            Object      = "Bus" / "White car" / "Limo" / "Truck" / "Blue car"/ "Green bus"  / "Bluewhite car" / "Oil truck" / "Pickup" / "Brown car"
            Place       = "Corn field" / "Highway" / "Dirt road"
            """)
   
   script_elements_BTTF = ("""
       Actor            = "Marty" / "George" / "Biff" / "Lou" / "Goldie" / "Match" / "Skinhead" / "3D"
       Object           = "Coffee"/ "Bar" / "Car" / "Sign" / "Directory" / "Phonebooth"
       Place            = "Cafe" 
       """)
   
   script_elements_ToE = ("""
       Actor            = "Mike" / "Susan" / "Linnekar" / "Blonde" / "Sanchez" / "Immigration official" / "Customs official" 
       Object           = "Car" / "Bomb" / "Building" / "Checkpost" 
       Place            =  "Border control" / "Main street" / "Parking lot" / "Left side street" 
       """)
   
   script_elements_Rope = ("""
       Actor            = "Philip" / "Brandon" / "Wilson" / "Rupert" / "Atwater" / "Kenneth" / "Janet" / "Kentley"
       Object           = "Glass"
       Place            = "Salon" 
       """)

   from_script = {
      "Rope" : script_elements_Rope ,
      "TouchOfEvil" : script_elements_ToE ,
      "BackToTheFuture" : script_elements_BTTF ,
      "NorthByNorthwest": script_elements_NNW
      }
   script_elements = from_script.get(input_script)
   
     
   psl_grammar = Grammar(main+script_elements)
   parse_tree = psl_grammar.parse(psl_sentence)

   #print(parse_tree)

   def print_tree(node,level):       
       if node.expr_name in ["Shot", "Timing","Cue","CameraTo","CameraWith","Development","Continuation","Recomposition","FollowEvent", "Composition","WhileEvent","ThenEvent","Event"]:

           if node.text.strip():              
              print(level,node.expr_name,node.text)

   
       for child in node.children:
          print_tree(child,level+'->')
          
   
   print_tree(parse_tree,"")   
   print("") 
   
 

#help(NodeVisitor)


input_movie = input("Please enter the name of the movie annotated (Rope, TouchOfEvil , BackToTheFuture or NorthByNorthwest):")
input_srt = input_movie

file = open( input_srt +".srt", "r")
lines = file.readlines()
file.close()

text = ''
for line in lines:
   if True: 
       text += line.rstrip('\n') + ' ' 
       psl_input = line.rstrip('\n')
       #print (psl_input)

    
#print(text)
PSL_parse(text,input_movie)


 

    