Code and examples for the paper 

"The Prose Storyboard Language: A Tool for Annotating and Directing Movies"
by Remi Ronfard, Vineet Gandhi, Laurent Boiron and Vaishnavi Ameya Murukutla.

PSL parser contains the grammar for Prose Storyboard Language and the parser written using the Parsimonious python library.

The necessary libraries that need to be installed for the code to run are:
Parsimonious (https://pypi.org/project/parsimonious/)

PSL parser takes .srt files as input. When run, the user is prompted to enter the name of the movie for which the PSL annotations are written. Then allows the parser to import the corresponding script elements from that movie. Then input the name of the .srt file in which the annotations have been written.

The parser will then generate the parsed output for each of the PSL sentence.

If you find our work useful, please cite our paper as follows.

@misc{ronfard2020prose,
      title={The Prose Storyboard Language: A Tool for Annotating and Directing Movies}, 
      author={Remi Ronfard and Vineet Gandhi and Laurent Boiron and Vaishnavi Ameya Murukutla},
      year={2020},
      eprint={1508.07593},
      archivePrefix={arXiv},
      primaryClass={cs.GR}
}
