1
00:00:10 --> 00:00:17 
fade in to CU Sanchez Bomb front

2
00:00:17 --> 00:00:21
then as Sanchez turns left pan left to ELS Linnekar Blonde front 

3
00:00:21 --> 00:00:23
then as Sanchez enters screen right and Sanchez crosses over Linnekar Blonde 

4
00:00:23 --> 00:00:24
hold to MS Sanchez back ELS Linnekar Blonde 34right

5
00:00:24 --> 00:00:27
then as Sanchez exits screen bottom right dolly right to Building 

6
00:00:27 --> 00:00:31
then as Sanchez enters screen bottom right dolly right to LS Car 34backleft LS Sanchez 34backleft

7
00:00:31 --> 00:00:32
then as Sanchez touches Car dolly in to FS Sanchez back

8
00:00:32 --> 00:00:34
then as Sanchez exits screen bottom right crane up to high angle FS Car 34backleft

9
00:00:34 --> 00:00:37
then as Linnekar Blonde enters screen top left crane up to high angle LS Linnekar Blonde 34right

10
00:00:37 --> 00:00:39
then as Linnekar crosses over Car and Blonde crosses under Car crane up left with Linnekar Blonde

11
00:00:39 --> 00:00:45
then as Linnekar turns right and Blonde turns left slow dolly left to high angle LS Linnekar Blonde 34backleft 

12
00:00:45 --> 00:00:47
then as Car moves to screen top left crane up left to high angle LS Car 34backleft

13
00:00:47 --> 00:00:53
then as Car turns left and Car crosses under Building dolly left to Building

14
00:00:53 --> 00:01:00
then as Car moves to Main street and Car turns towards camera crane down left to LS Car front

15
00:01:00 --> 00:01:03
then as Car stops at Main street crane up to high angle LS Car front

16
00:01:03 --> 00:01:14
then dolly out to high angle ELS Car 34left

17
00:01:14 --> 00:01:21
then as Car moves to screen bottom center dolly out to high angle LS Car 34left

18
00:01:21 --> 00:01:23
then as Car stops near Left side street crane up left to high angle LS Car front

19
00:01:23 --> 00:01:25
then as Susan Mike enters screen right crane down to  high angle LS Car front LS Susan Mike 34left

20
00:01:25 --> 00:01:27
then as Susan Mike crosses over Car crane down to high angle LS Susan Mike 34left LS Car 34right

21
00:01:27 --> 00:01:31 
then as Susan Mike moves to Left side street crane down left to FS Susan Mike 34left LS Car 34right

22
00:01:31 --> 00:01:35
then as Car turns right and Car moves to screen bottom left dolly left to LS Susan Mike 34left MS Car 34left

23
00:01:35 --> 00:01:39
then as Car crosses over Susan Mike and Car exits screen bottom left dolly left to LS Susan Mike 34left

24
00:01:39 --> 00:01:47
then as Susan Mike moves to screen bottom left dolly left with Susan Mike

25
00:01:47 --> 00:01:49 
then as Susan Mike moves to Car dolly left to MLS Car 34left LS Susan Mike front

26
00:01:49 --> 00:01:56
then as Susan Mike crosses over Car dolly left to LS Susan Mike 34left LS Car 34left

27
00:01:56 --> 00:02:01
then as Susan Mike moves to screen bottom center and Car moves to screen bottom right dolly left to FS Susan Mike 34left LS Car 34right

28
00:02:01 --> 00:02:08
then as Car moves to screen bottom left crane up left to high angle FS Susan Mike 34left LS Car 34left

29
00:02:08 --> 00:02:14
then as Car crosses under Susan Mike dolly left to high angle LS Car 34left LS Susan Mike 34left

30
00:02:14 --> 00:02:22
then as Car crosses under Checkpost and Susan Mike crosses over Checkpost dolly left to high angle LS Car front ELS Susan Mike front

31
00:02:22 --> 00:02:31
then as Car moves to screen bottom center and Susan Mike moves to screen bottom right dolly left with Car Susan Mike

32
00:02:31 --> 00:02:34
then as Immigration official speaks to Susan Mike crane down to high angle FS Car 34left FS Susan Mike front MLS Immigration official 34backleft

33
00:02:34 --> 00:02:36
then as Car stops at Border control and Mike crosses under Susan crane down to 

34
00:02:36 --> 00:02:37
high angle MLS Car 34left MLS Mike Susan front MLS Customs official 34left MLS Immigration official 34backleft

35
00:02:37 --> 00:02:39 
then as Mike turns right and Customs official crosses under Susan hold to 

36
00:02:39 --> 00:02:41
MLS Car 34left MLS Mike right Customs official 34left Susan front MLS Immigration official 34backleft

37
00:02:41 --> 00:02:44
then as Customs official moves to screen top center and Customs official turns left hold to 

38
00:02:44 --> 00:02:50
MLS Car 34left MLS Mike right LS Customs official 34left MLS Susan front MLS Immigration official 34backleft

39
00:02:50 --> 00:02:54
then as Susan crosses over Customs official and Mike Susan exits screen bottom left hold to MLS Car 34left LS Customs official 34left MLS Immigration official left

40
00:02:54 --> 00:02:56
then as Customs official crosses under Linnekar Blonde and Immigration official moves to Linnekar dolly left to

41
00:02:56 --> 00:02:57
FS Customs official 34left MLS Blonde Linnekar 34left MLS Immigration official 34backleft

42
00:02:57 --> 00:03:01
then as Mike Susan enters screen left and Susan crosses under Mike hold to 

43
00:03:01 --> 00:03:03
FS Mike Susan 34right FS Customs official 34left MLS Blonde Linnekar 34left MLS Immigration official 34backleft

44
00:03:03 --> 00:03:08
then as Mike Susan crosses under Customs official hold to FS Customs official front MLS Blonde 34right Linnekar 34left Immigration official 34left

45
00:03:11 --> 00:03:15
then as Car moves to screen bottom left and Mike Susan moves to screen left dolly left to MS Car left LS Mike Susan 34left FS Customs official 34left

46
00:03:15 --> 00:03:22
then as Car exits screen bottom left and Customs official exits screen right dolly in to MLS Mike Susan 34left

47
00:03:22 --> 00:03:28
then as Susan crosses over Mike and Susan turns right dolly in to MS Susan right Mike left
